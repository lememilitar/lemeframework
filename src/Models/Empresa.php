<?php
namespace App\Models;

class Empresa
{
    public static function all()
    {
        $empresas = [
            ['database' => 'lemepalma', 'company_name' => 'Leme Palma'],
            ['database' => 'lemebomfim', 'company_name' => 'Farda Fácil'],
            ['database' => 'omilitar', 'company_name' => 'O Militar'],
            ['database' => 'bazar', 'company_name' => 'Bazar Militar'],
        ];

        return $empresas;
    }
}
