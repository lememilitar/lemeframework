<?php

class Conexao{
    private $user, $pass, $host, $dbtype, $dbname;
    private $pdo;
    private $dbopts;

    function __construct($user=null, $pass=null, $host=null, $dbname=null){
        $this->dbopts = parse_url(getenv('DATABASE_URL'));
        $this->user = $this->dbopts['user']=='' ? $user : $this->dbopts['user'];
        $this->pass = $this->dbopts['pass']=='' ? $user : $this->dbopts['pass'];
        $this->host = $this->dbopts['host']=='' ? $user : $this->dbopts['host'];
        $this->dbname = ltrim($this->dbopts["path"],'/')=='' ? $user : ltrim($this->dbopts["path"],'/');
    }

    function Conectar(){
        try{
            if(PHP_OS=='WINNT' || PHP_OS=='WIN32'){
                $this->pdo = new PDO($this->getDbtype().':Database='.$this->getDbname().';server='.$this->getHost(),$this->getUser(),$this->getPass());
            }else{
                $this->pdo = new PDO($this->getDbtype().':host='.$this->getHost().';dbname='.$this->getDbname(), $this->getUser(),$this->getPass());
            }
            return $this->pdo;
        }catch(PDOException $e){
            echo 'Error: '.$e->getMessage()."\n";
            echo 'Code Error: '.$e->getCode()."\n";
            return null;
        }
    }


    public function executeQuery($query){
        if(strpos($query,'select')===0){
            return $this->pdo->query($query);
        }else{
            return $this->pdo->exec($query);
        }
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function getPass() {
        return $this->pass;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function getHost() {
        return $this->host;
    }

    public function setHost($host) {
        $this->host = $host;
    }

    public function getDbtype() {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            if(version_compare(phpversion(), '5.3.10', '>=')){
                $this->dbtype = 'sqlsrv';
            }else{
                $this->dbtype = 'mssql';
            }
        }else{
            $this->dbtype = 'dblib';
        }
        $this->dbtype = 'pgsql';
        return $this->dbtype;
    }

    public function setDbtype($dbtype) {
        $this->dbtype = $dbtype;
    }

    public function getDbname() {
        return $this->dbname;
    }

    public function setDbname($dbname) {
        $this->dbname = $dbname;
    }

    public function getPdo() {
        return $this->pdo;
    }

    public function setPdo($pdo) {
        $this->pdo = $pdo;
    }

}
?>
