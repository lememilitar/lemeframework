<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author George Moura
 */
class Usuario{
    private $user;
    private $pass;
    private $email;
    private $nome;
    private $codfornecedor;
    private $empresa;
    private $setor;
    private $conn;
    private $attributes;

    function __Construct(){
        $this->attributes = get_object_vars($this);
    }

    function getUser(){
        return $this->user;
    }
    function getPass(){
        return $this->pass;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getEmpresa() {
        return $this->empresa;
    }
    public function getSetor() {
        return $this->setor;
    }
    public function getNome() {
        return $this->nome;
    }
    public function getAttributes(){
        $vars = get_object_vars($this);
        unset($vars['attributes']);
        unset($vars['pass']);
        unset($vars['conn']);
        return $vars;
    }


    function setUser($user){
        $user = trim($user);
        $user = addslashes($user);
        $this->user = $user;
    }
    function setPass($pass){
        $pass = trim($pass);
        $pass = addslashes($pass);
        $this->pass = $pass;
    }
    function setEmail($email){
        $this->email = strtolower($email);
    }
    public function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }
    public function setSetor($setor) {
        $this->setor = $setor;
    }
    public function setNome($nome) {
        $this->nome = $nome;
    }
    public function getCodfornecedor() {
        return $this->codfornecedor;
    }

    public function setCodfornecedor($codfornecedor) {
        $this->codfornecedor = $codfornecedor;
    }


    function logar(){
        $this->conn = new Conexao();
        //echo 'Empresa:'.$this->empresa;
        $this->conn->setDbname($this->empresa);
        $db = $this->conn->Conectar();
        //var_dump($db);
        $usuario=strtoupper($this->user);
        $senha=strtoupper($this->pass);
        //pesquisa Nome e Senha no Banco de Dados  da Tebla Comercial
        $sql = "SELECT Nome,Senha FROM Usr_Comerc WHERE Nome='{$usuario}' and Senha='{$senha}'";
        $stmt=$db->query($sql);
        //echo $sql.'<hr/>';
        //var_dump($stmt);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        //var_dump($user);
        $conta=count($user);
        if($user){
            $this->obterUsuario();
            return $this->getAttributes();
            //session_start();
            //$_SESSION['user']=$usuario;
            //header('Location: index.php');
            //echo 'logou - usuario:'.$usuario.' e senha: '.$senha.' e $conta='.$conta;
        }else{
            $sql = "SELECT Nome,Senha FROM Usr_Financ WHERE Nome='{$usuario}' and Senha='{$senha}'";
            $stmt=$db->query($sql);
            //var_dump($stmt);
            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            //var_dump($user);
            $conta=count($user);
            if($user){
                $this->obterUsuario();
                return $this->getAttributes();
                //session_start();
                //$_SESSION['user']=$usuario;
                //header('Location: index.php');
            }else{
                //pesquisa Nome e Senha no Banco de Dados  da Tebla PDV
                $sql = "SELECT Nome,Senha FROM Usr_PDV WHERE Nome='{$usuario}' and Senha='{$senha}'";
                $stmt=$db->query($sql);
                $user=$stmt->fetch(PDO::FETCH_ASSOC);
                //var_dump($user);
                $conta=count($user);
                if($user){
                    $this->obterUsuario();
                    return $this->getAttributes();
                }else{
                    return $this->attributes;
                }
            }
        }
    }

    function gravarLog(){

    }

    function cadastrarUsuario(Usuario $usuario){
        $this->conn = new Conexao();

        $bd = $this->conn->Conectar();
        $stmt = $bd->prepare("insert into usuarios
            (Nome,Senha,Email,Empresa)
            values
            (?,?,?,?)");
        $stmt->bindValue(1, $usuario->getUser());
        $stmt->bindValue(2, $usuario->getPass());
        $stmt->bindValue(3, $usuario->getEmail());
        $stmt->bindValue(4, $usuario->getEmpresa());

        return $stmt->execute();

    }

    function listarEmpresas(){
        $empresas = '<select name="empresa">';

        foreach (\App\Models\Empresa::all() as $obj) {
            $empresas .= '<option value="'.$obj['database'].'">'.$obj['company_name'].'</option>';
        }

        $empresas .= '</select>';

        return $empresas;
    }

    public function exibirFormLogin(){
        $form = '<form action="autentica.php" method="POST">
                    <table>
                            <tr>
                                    <td colspan="2"><p style="color:red;">';
                                        if(isset($_GET['msg'])) $form .= $_GET['msg'].'</p>';
                            $form.='</td>
                            </tr>
                            <tr>
                                    <td>Usuário: </td><td><input type="text" name="user" /></td>
                            </tr>
                            <tr>
                                    <td>Senha: </td><td><input type="password" name="pass" /></td>
                            </tr>
                            <tr>
                                <td>Empresa:</td><td>'.$this->listarEmpresas().'</td>
                            </tr>
                            <tr>
                                    <td colspan="2"><input type="submit" value="Entar"></td>
                            </tr>
                    </table>
                 </form>';
        return $form;
    }

    public function exibirFormCadastro(Usuario $usuario = NULL){
        echo '
            Cadastre-se Agora<br/><br/>
            <form action="inserir.php" method="POST">
                <input type="hidden" name="acao" value="usuario">
                <table>
                    <tr>
                        <td>Usuário: </td><td><input type="text" name="usuario" value="'.$usuario->getUser().'" /></td>
                    </tr>
                    <tr>
                        <td>Senha: </td><td><input type="password" name="senha" value="" /></td>
                    </tr>
                    <tr>
                        <td>E-mail: </td><td><input type="email" name="email" value="'.$usuario->getEmail().'" /></td>
                    </tr>
                    <tr>
                        <td>Empresa: </td>
                        <td>
                            '.$this->listarEmpresas().'
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="submit" value="Cadastrar"></td>
                    </tr>
                </table>

            </form>';
    }

    public function obterUsuario(){
        $this->conn = new Conexao();
        $bd = $this->conn->Conectar();
        $query = "select Fornecedor,Nome,Fantasia,Email,TipoFornecedor,Observacao from Fornecedor where (TipoFornecedor=4 or TipoFornecedor=3) and LOWER(Fantasia)='{$this->getUser()}'";
        $stmt=$bd->query($query);
        //var_dump($stmt);
        //$stmt = $this->conn->executeQuery($query);
        if ($stmt) {
            while($usuarios = $stmt->fetchObject()){
                //var_dump($usuarios);
                $this->setCodfornecedor($usuarios->fornecedor);
                $this->setNome($usuarios->nome);
                $this->setEmail($usuarios->email);
            }
        }
    }

}


?>
