<?php
require_once 'vendor/autoload.php';
include_once 'src/Conexao.php';

$dotenv = new Dotenv\Dotenv(__DIR__, '../.env');
$dotenv->load();

class ConexaoTest extends PHPUnit_Framework_TestCase
{
  function testConectar(){
    $c = new Conexao;
    $c->setDbname('bazar');
    $pdo = $c->Conectar();
    $this->assertInstanceOf(get_class($pdo), $pdo, 'Não é pdo');

    $c2 = new Conexao;
    $c2->setHost('192.168.1.18');
    $c2->setPass('postgres');
    $pdo = $c2->Conectar();
    $this->assertNull($pdo,'Não é null');
  }

  function testExecuteQuery(){
    $c = new Conexao;
    $c->setDbname('bazar');
    $pdo = $c->Conectar();
    $stmt = $c->executeQuery("show databases");
    $this->assertEquals(0, $stmt);

    $stmt2  = $c->executeQuery("select 1");
    $this->assertInstanceOf('PDOStatement', $stmt2, 'Não é False');
  }

  function testSetUser(){
    $c = new Conexao;
    $c->setUser('sa');
    $u = $c->getUser();
    $this->assertInternalType('string', $u);
  }

  function testSetPass(){
    $c = new Conexao;
    $c->setPass('12345');
    $p = $c->getPass();
    $this->assertInternalType('string', $p);
  }

  function testSetHost(){
    $c = new Conexao;
    $c->setHost('192.168.1.1000');
    $p = $c->getHost();
    $this->assertInternalType('string', $p);
  }

  function testDbType(){
    $c = new Conexao;
    $c->setDbtype('sqlsrv');
    $p = $c->getDbtype();
    $this->assertInternalType('string', $p);
  }

  function testDbName(){
    $c = new Conexao;
    $c->setDbname('lemepalma');
    $p = $c->getDbname();
    $this->assertInternalType('string', $p);
  }

  function testSetPdo(){
    $c = new Conexao;
    $c->setDbname('bazar');

    $pdo = $c->Conectar();

    $c->setPdo($pdo);
    $p = $c->getPdo();
    $this->assertInstanceOf(get_class($pdo), $pdo, 'message');;
  }
}
