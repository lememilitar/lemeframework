<?php

class AutoLoad
{
  function __construct()
  {
    include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'src/Conexao.php';
    include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'src/Models/Empresa.php';
    include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'src/Usuario.php';
    include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'src/LemeMail.php';
  }
}
